airfoilName st14.dat
Re 	790000
M 	5.000000e-01
N 	4.500000e+00
VACC 	0 VACCflag 1
XTR 	1.00 	1.00 XTRflag 0
alpha,cl,cd,cdp,cm
-30,-1.6434,0.54948,0.54785,0.3459
-29.9,-1.6393,0.5465,0.54487,0.3432
-29.8,-1.6339,0.54386,0.54223,0.3395
-29.7,-1.63,0.54067,0.53904,0.3371
-29.6,-1.6262,0.53752,0.53589,0.3347
-29.5,-1.6223,0.53443,0.53279,0.3323
-29.4,-1.6184,0.53135,0.52971,0.3299
-29.3,-1.6145,0.52828,0.52664,0.3275
-29.2,-1.6106,0.52523,0.52358,0.3252
-29.1,-1.6067,0.52222,0.52057,0.3228
-29,-1.6027,0.51923,0.51758,0.3204
-28.9,-1.5988,0.51627,0.51462,0.3181
-28.8,-1.5948,0.51332,0.51166,0.3157
-28.7,-1.5908,0.51039,0.50873,0.3134
-28.6,-1.5868,0.50749,0.50584,0.3111
-28.5,-1.5828,0.5046,0.50294,0.3087
-28.4,-1.5788,0.50174,0.50007,0.3064
-28.3,-1.5747,0.49888,0.49722,0.3041
-28.2,-1.5707,0.49606,0.49439,0.3018
-28.1,-1.5666,0.49323,0.49157,0.2995
-28,-1.5625,0.49042,0.48875,0.2972
-27.9,-1.5584,0.48763,0.48596,0.2949
-27.8,-1.5543,0.48487,0.4832,0.2927
-27.7,-1.5502,0.48211,0.48043,0.2904
-27.6,-1.546,0.47935,0.47767,0.2881
-27.5,-1.5418,0.47662,0.47494,0.2859
-27.4,-1.5377,0.47388,0.4722,0.2836
-27.3,-1.5335,0.47117,0.46948,0.2814
-27.2,-1.5293,0.46846,0.46676,0.2792
-27.1,-1.525,0.46576,0.46407,0.2769
-27,-1.5208,0.46308,0.46139,0.2747
-26.9,-1.5166,0.46039,0.4587,0.2725
-26.8,-1.5123,0.45774,0.45604,0.2703
-26.7,-1.508,0.45508,0.45338,0.2681
-26.6,-1.5037,0.45242,0.45072,0.2659
-26.5,-1.4994,0.44978,0.44808,0.2637
-26.4,-1.4951,0.44714,0.44543,0.2615
-26.3,-1.4907,0.44451,0.44281,0.2594
-26.2,-1.4864,0.44189,0.44019,0.2572
-26.1,-1.482,0.43928,0.43757,0.2551
-26,-1.4776,0.43667,0.43496,0.2529
-25.9,-1.4732,0.43433,0.43262,0.2507
-25.8,-1.4688,0.43171,0.43,0.2486
-25.7,-1.4644,0.42912,0.42741,0.2465
-25.6,-1.4599,0.42653,0.42481,0.2444
-25.5,-1.4554,0.42394,0.42222,0.2423
-25.4,-1.4509,0.42137,0.41965,0.2402
-25.3,-1.4465,0.41881,0.41709,0.2381
-25.2,-1.442,0.41625,0.41452,0.236
-25.1,-1.4374,0.41371,0.41198,0.2339
-25,-1.4329,0.41115,0.40943,0.2318
-24.9,-1.4284,0.40863,0.40689,0.2297
-24.8,-1.4238,0.40611,0.40438,0.2277
-24.7,-1.4192,0.40359,0.40185,0.2256
-24.6,-1.4147,0.40109,0.39935,0.2236
-24.5,-1.4101,0.39859,0.39685,0.2215
-24.4,-1.4055,0.3961,0.39436,0.2195
-24.3,-1.4009,0.39362,0.39187,0.2175
-24.2,-1.3962,0.39114,0.38939,0.2155
-24.1,-1.3916,0.38869,0.38694,0.2134
-24,-1.387,0.38623,0.38449,0.2114
-23.9,-1.3823,0.38377,0.38202,0.2094
-23.8,-1.3776,0.38134,0.37958,0.2074
-23.7,-1.3729,0.3789,0.37715,0.2054
-23.6,-1.3682,0.37647,0.37472,0.2034
-23.5,-1.3635,0.37405,0.3723,0.2014
-23.4,-1.3587,0.37163,0.36988,0.1994
-23.3,-1.354,0.36923,0.36747,0.1975
-23.2,-1.3492,0.36683,0.36507,0.1955
-23.1,-1.3444,0.36444,0.36268,0.1935
-23,-1.3396,0.36204,0.36028,0.1915
-22.9,-1.3348,0.35965,0.35789,0.1895
-22.8,-1.3299,0.35726,0.35549,0.1874
-22.7,-1.3249,0.35483,0.35306,0.1853
-22.5,-1.3131,0.34952,0.34776,0.1803
-22.4,-1.3085,0.34697,0.34522,0.1785
-22.3,-1.3039,0.34448,0.34272,0.1767
-22.2,-1.2992,0.34202,0.34025,0.175
-22.1,-1.2945,0.33959,0.33782,0.1732
-22,-1.2899,0.3372,0.33543,0.1714
-21.9,-1.2852,0.33484,0.33307,0.1697
-21.8,-1.2805,0.3325,0.33073,0.1679
-21.7,-1.2758,0.33018,0.32841,0.1662
-21.6,-1.2712,0.32789,0.32611,0.1645
-21.5,-1.2665,0.32562,0.32384,0.1627
-21.4,-1.2617,0.32336,0.32159,0.161
-21.3,-1.257,0.32113,0.31935,0.1593
-21.2,-1.2523,0.31891,0.31713,0.1576
-21.1,-1.2476,0.31671,0.31493,0.1559
-21,-1.2428,0.31451,0.31273,0.1542
-20.9,-1.2381,0.31233,0.31055,0.1525
-20.8,-1.2333,0.31015,0.30837,0.1508
-20.7,-1.2285,0.30799,0.30621,0.1491
-20.6,-1.2237,0.30582,0.30404,0.1475
-20.4,-1.2141,0.30186,0.30008,0.1441
-20.3,-1.2093,0.2997,0.29792,0.1424
-20.2,-1.2044,0.29756,0.29577,0.1408
-20.1,-1.1995,0.29541,0.29363,0.1391
-20,-1.1947,0.29328,0.29149,0.1375
-19.9,-1.1898,0.29115,0.28936,0.1359
-19.8,-1.1849,0.28903,0.28724,0.1343
-19.7,-1.18,0.28691,0.28512,0.1326
-19.6,-1.1751,0.28479,0.283,0.131
-19.5,-1.1702,0.2827,0.2809,0.1294
-19.4,-1.1653,0.2806,0.2788,0.1279
-19.3,-1.1604,0.27851,0.27671,0.1263
-19.2,-1.1555,0.27642,0.27462,0.1247
-19.1,-1.1506,0.27434,0.27254,0.1231
-19,-1.1456,0.27226,0.27046,0.1216
-18.9,-1.1407,0.27019,0.2684,0.12
-18.8,-1.1358,0.26813,0.26633,0.1185
-18.7,-1.1308,0.26607,0.26427,0.1169
-18.6,-1.1258,0.26401,0.26221,0.1154
-18.5,-1.1209,0.26197,0.26017,0.1139
-18.4,-1.1159,0.25992,0.25812,0.1124
-18.3,-1.1109,0.25788,0.25608,0.1108
-18.2,-1.1059,0.25585,0.25405,0.1093
-18.1,-1.1009,0.25382,0.25202,0.1078
-18,-1.0959,0.25179,0.24999,0.1063
-17.9,-1.0909,0.24977,0.24797,0.1048
-17.8,-1.0858,0.24775,0.24595,0.1033
-17.7,-1.0808,0.24573,0.24393,0.1018
-17.6,-1.0757,0.24371,0.24191,0.1003
-17.5,-1.0706,0.2417,0.23989,0.0988
-17.4,-1.0655,0.23967,0.23787,0.0973
-17.3,-1.0603,0.23765,0.23584,0.0957
-17.2,-1.0551,0.2356,0.2338,0.0942
-17.1,-1.0499,0.23356,0.23175,0.0927
-17,-1.0446,0.23148,0.22966,0.0912
-16.9,-1.0393,0.22937,0.22756,0.0896
-16.8,-1.034,0.22723,0.22541,0.0881
-16.7,-1.0286,0.22499,0.22318,0.0865
-16.4,-1.0095,0.21772,0.21591,0.0811
-16.3,-1.0047,0.21569,0.21388,0.0798
-16.2,-0.9998,0.21368,0.21187,0.0786
-16.1,-0.995,0.21169,0.20989,0.0773
-16,-0.9901,0.20973,0.20792,0.076
-15.9,-0.9852,0.20779,0.20598,0.0747
-15.8,-0.9803,0.20587,0.20406,0.0735
-15.7,-0.9754,0.20396,0.20216,0.0722
-15.6,-0.9705,0.20208,0.20028,0.0709
-15.5,-0.9656,0.20039,0.19858,0.0696
-15.4,-0.9606,0.19854,0.19674,0.0683
-15.3,-0.9555,0.19671,0.19491,0.0671
-15.2,-0.9504,0.1949,0.19309,0.0658
-15.1,-0.9453,0.19309,0.19129,0.0646
-15,-0.9402,0.1913,0.1895,0.0633
-14.9,-0.9351,0.18953,0.18772,0.062
-14.8,-0.9299,0.18775,0.18595,0.0608
-14.7,-0.9246,0.18599,0.18419,0.0595
-14.6,-0.9194,0.18423,0.18244,0.0583
-14.5,-0.9141,0.18249,0.18069,0.057
-14.4,-0.9089,0.18076,0.17896,0.0558
-14.3,-0.9036,0.17903,0.17723,0.0545
-14.2,-0.8982,0.17732,0.17551,0.0533
-14.1,-0.8929,0.17561,0.17381,0.052
-14,-0.8875,0.17391,0.17211,0.0508
-13.9,-0.8822,0.17222,0.17042,0.0496
-13.8,-0.8768,0.17054,0.16874,0.0484
-13.7,-0.8714,0.16887,0.16707,0.0471
-13.6,-0.866,0.1672,0.16541,0.0459
-13.5,-0.8605,0.16555,0.16376,0.0447
-13.4,-0.8551,0.1639,0.16211,0.0435
-13.3,-0.8496,0.16227,0.16047,0.0423
-13.2,-0.8441,0.16063,0.15884,0.0411
-13.1,-0.8385,0.159,0.15721,0.0399
-13,-0.833,0.15738,0.15559,0.0387
-12.9,-0.8274,0.15576,0.15397,0.0375
-12.8,-0.8217,0.15414,0.15235,0.0362
-12.7,-0.8161,0.15253,0.15075,0.035
-12.6,-0.8104,0.15094,0.14915,0.0339
-12.5,-0.8048,0.14934,0.14756,0.0327
-12.4,-0.799,0.14776,0.14597,0.0315
-12.3,-0.7933,0.14618,0.1444,0.0303
-12.2,-0.7876,0.14461,0.14283,0.0291
-12.1,-0.7819,0.14304,0.14126,0.0279
-12,-0.7761,0.14148,0.1397,0.0267
-11.9,-0.7703,0.13993,0.13815,0.0256
-11.8,-0.7644,0.13838,0.13661,0.0244
-11.7,-0.7586,0.13683,0.13506,0.0232
-11.6,-0.7527,0.13528,0.13351,0.022
-11.5,-0.7467,0.13374,0.13197,0.0208
-11.4,-0.7408,0.13221,0.13044,0.0196
-11.3,-0.7348,0.13068,0.12891,0.0185
-11.2,-0.7288,0.12916,0.12739,0.0173
-11.1,-0.7228,0.12764,0.12588,0.0161
-11,-0.7168,0.12613,0.12436,0.015
-10.9,-0.7108,0.12461,0.12285,0.0138
-10.8,-0.7047,0.12309,0.12133,0.0126
-10.7,-0.6987,0.12158,0.11983,0.0115
-10.6,-0.6926,0.12006,0.11831,0.0103
-10.5,-0.6864,0.11854,0.11679,0.0092
-10.4,-0.6795,0.11641,0.11466,0.0077
-10.3,-0.6734,0.11488,0.11314,0.0066
-10.2,-0.667,0.11332,0.11158,0.0054
-10.1,-0.6605,0.11177,0.11003,0.0042
-10,-0.6541,0.11028,0.10854,0.003
-9.9,-0.6473,0.10876,0.10703,0.0018
-9.7,-0.6344,0.10591,0.1042,-0.0005
-9.6,-0.6288,0.1046,0.10289,-0.0015
-9.5,-0.6233,0.10331,0.10161,-0.0024
-9.4,-0.6178,0.10205,0.10036,-0.0033
-9.3,-0.6126,0.10081,0.09912,-0.0042
-9.2,-0.6075,0.09961,0.09793,-0.005
-9.1,-0.6027,0.09845,0.09678,-0.0057
-9,-0.5958,0.09713,0.09546,-0.007
-8.9,-0.5867,0.09564,0.09397,-0.009
-8.8,-0.5772,0.09416,0.09248,-0.011
-8.7,-0.5677,0.0927,0.09102,-0.013
-8.6,-0.5584,0.09129,0.08961,-0.0149
-8.5,-0.5487,0.08988,0.0882,-0.0169
-8.4,-0.5385,0.08845,0.08677,-0.019
-8.3,-0.5293,0.08713,0.08545,-0.0209
-8.2,-0.5198,0.08581,0.08413,-0.0227
-8.1,-0.5098,0.08448,0.08279,-0.0248
-8,-0.4996,0.08315,0.08146,-0.0268
-7.9,-0.4909,0.08194,0.08025,-0.0284
-7.8,-0.4817,0.08071,0.07902,-0.0301
-7.7,-0.472,0.07947,0.07776,-0.032
-7.6,-0.4641,0.07836,0.07665,-0.0334
-7.5,-0.455,0.0772,0.07548,-0.0351
-7.4,-0.4449,0.07599,0.07426,-0.037
-7.3,-0.4347,0.07479,0.07305,-0.0389
-7.2,-0.4241,0.07356,0.07181,-0.0409
-7.1,-0.4131,0.07233,0.07057,-0.043
-7,-0.4021,0.07111,0.06933,-0.045
-6.9,-0.3906,0.06986,0.06806,-0.0472
-6.8,-0.3791,0.06862,0.0668,-0.0493
-6.7,-0.3669,0.06733,0.0655,-0.0515
-6.6,-0.3547,0.06605,0.0642,-0.0537
-6.5,-0.3421,0.06475,0.06287,-0.056
-6.4,-0.329,0.06342,0.06152,-0.0584
-6.3,-0.3157,0.06208,0.06016,-0.0608
-6.2,-0.302,0.0607,0.05876,-0.0633
-6.1,-0.2881,0.05931,0.05734,-0.0658
-6,-0.2737,0.05788,0.05588,-0.0683
-5.9,-0.2587,0.05639,0.05437,-0.071
-5.8,-0.2435,0.05486,0.05281,-0.0737
-5.7,-0.228,0.05326,0.05117,-0.0765
-5.6,-0.2116,0.05151,0.0494,-0.0795
-5.5,-0.1942,0.04941,0.04726,-0.083
-5.4,-0.1757,0.04688,0.04467,-0.0869
-5.3,-0.1587,0.04552,0.04328,-0.0897
-5.1,-0.1248,0.04337,0.04107,-0.0944
-5,-0.1075,0.04206,0.03971,-0.0969
-4.9,-0.0901,0.04073,0.03833,-0.0993
-4.8,-0.0721,0.03933,0.03689,-0.1018
-4.6,-0.0356,0.03636,0.03379,-0.1068
-4.5,-0.0164,0.03466,0.03203,-0.1095
-4.4,0.0037,0.0327,0.02999,-0.1125
-4.3,0.0257,0.03008,0.02725,-0.1162
-4.2,0.0591,0.02246,0.01919,-0.1255
-4,0.0981,0.01365,0.00947,-0.1334
-3.9,0.1136,0.0125,0.00805,-0.134
-3.8,0.1288,0.01174,0.00709,-0.1343
-3.7,0.1438,0.01117,0.00634,-0.1344
-3.6,0.1589,0.01072,0.00573,-0.1345
-3.5,0.1737,0.01035,0.00523,-0.1344
-3.4,0.1886,0.01006,0.00481,-0.1344
-3.3,0.2033,0.00982,0.00447,-0.1342
-3.2,0.2178,0.00965,0.00423,-0.1341
-3.1,0.232,0.00952,0.00403,-0.1339
-3,0.2462,0.00941,0.00382,-0.1337
-2.9,0.2603,0.00931,0.00364,-0.1335
-2.8,0.2749,0.00919,0.00346,-0.1334
-2.6,0.3034,0.00904,0.00325,-0.1331
-2.5,0.3175,0.00898,0.00312,-0.1329
-2.4,0.3314,0.00895,0.00303,-0.1327
-2.3,0.3454,0.00888,0.0029,-0.1326
-2.2,0.3596,0.00881,0.00281,-0.1325
-2.1,0.3737,0.00876,0.00272,-0.1324
-2,0.3874,0.00875,0.00264,-0.1322
-1.9,0.4014,0.00872,0.00256,-0.132
-1.8,0.4151,0.00872,0.00249,-0.1319
-1.7,0.429,0.00868,0.00242,-0.1317
-1.6,0.4429,0.00866,0.00239,-0.1316
-1.5,0.4561,0.0087,0.00234,-0.1314
-1.4,0.4698,0.00871,0.0023,-0.1313
-1.3,0.4835,0.00872,0.00226,-0.1311
-1.2,0.4968,0.00874,0.00223,-0.131
-1.1,0.5101,0.00878,0.00222,-0.1308
-1,0.5237,0.0088,0.0022,-0.1307
-0.9,0.5366,0.00888,0.0022,-0.1305
-0.8,0.5502,0.0089,0.00218,-0.1304
-0.7,0.5638,0.00892,0.00218,-0.1303
-0.6,0.577,0.00898,0.00219,-0.1301
-0.5,0.5905,0.00901,0.00219,-0.13
-0.4,0.6039,0.00905,0.00219,-0.1299
-0.3,0.6173,0.00909,0.0022,-0.1298
-0.2,0.63,0.00919,0.00224,-0.1295
-0.1,0.643,0.00928,0.00226,-0.1294
0,0.6559,0.00938,0.00229,-0.1292
0.1,0.669,0.00944,0.00232,-0.129
0.2,0.6817,0.00954,0.00237,-0.1288
0.3,0.6947,0.00963,0.0024,-0.1287
0.4,0.7076,0.00973,0.00244,-0.1285
0.5,0.7204,0.00983,0.00249,-0.1283
0.6,0.7331,0.00995,0.00255,-0.1281
0.7,0.7443,0.01022,0.00265,-0.1277
0.8,0.7564,0.01041,0.00274,-0.1275
0.9,0.7684,0.0106,0.00284,-0.1272
1,0.7805,0.01078,0.00293,-0.127
1.1,0.7932,0.01092,0.00301,-0.1268
1.2,0.8061,0.01102,0.00308,-0.1266
1.3,0.8192,0.01111,0.00314,-0.1265
1.4,0.8319,0.01123,0.00323,-0.1263
1.5,0.8451,0.01131,0.00329,-0.1261
1.6,0.8583,0.01139,0.00336,-0.126
1.7,0.8716,0.01145,0.00342,-0.1258
1.8,0.8849,0.01152,0.00348,-0.1257
1.9,0.898,0.0116,0.00354,-0.1255
2,0.9113,0.01166,0.00361,-0.1254
2.1,0.9247,0.01172,0.00368,-0.1252
2.2,0.9377,0.01182,0.00375,-0.1251
2.3,0.9509,0.01189,0.00383,-0.1249
2.4,0.9643,0.01196,0.00389,-0.1247
2.5,0.9776,0.01202,0.00396,-0.1245
2.6,0.9909,0.01209,0.00403,-0.1244
2.7,1.0041,0.01216,0.00411,-0.1242
2.8,1.0174,0.01223,0.0042,-0.124
2.9,1.0308,0.01229,0.00427,-0.1238
3,1.0441,0.01236,0.00435,-0.1236
3.1,1.0572,0.01245,0.00443,-0.1234
3.2,1.0703,0.01253,0.00453,-0.1231
3.3,1.0838,0.01258,0.00461,-0.123
3.4,1.0891,0.01123,0.00481,-0.1216
3.5,1.1024,0.01132,0.0049,-0.1213
3.6,1.1157,0.01143,0.00499,-0.1211
3.7,1.1289,0.01154,0.00508,-0.1208
3.8,1.142,0.01165,0.00518,-0.1205
3.9,1.1554,0.01175,0.00527,-0.1202
4,1.1688,0.01185,0.00538,-0.1199
4.1,1.1818,0.01197,0.00548,-0.1196
4.2,1.195,0.01209,0.00558,-0.1192
4.3,1.2081,0.01222,0.0057,-0.1189
4.4,1.2214,0.01232,0.0058,-0.1186
4.5,1.2347,0.01243,0.00591,-0.1182
4.6,1.2479,0.01254,0.00602,-0.1178
4.7,1.2611,0.01266,0.00614,-0.1175
4.8,1.2741,0.01279,0.00626,-0.1171
4.9,1.2871,0.01293,0.00638,-0.1166
5,1.3002,0.01305,0.00651,-0.1162
5.1,1.3134,0.01318,0.00665,-0.1158
5.2,1.3265,0.0133,0.00677,-0.1153
5.3,1.3396,0.01344,0.00691,-0.1149
5.4,1.3525,0.01358,0.00705,-0.1144
5.5,1.3653,0.01374,0.0072,-0.1139
5.6,1.3785,0.01386,0.00734,-0.1134
5.7,1.3916,0.01399,0.00749,-0.1128
5.8,1.4046,0.01413,0.00764,-0.1123
5.9,1.4173,0.01429,0.00779,-0.1117
6,1.43,0.01445,0.00795,-0.1111
6.1,1.4427,0.01462,0.00813,-0.1105
6.2,1.4557,0.01476,0.0083,-0.1099
6.3,1.4685,0.01491,0.00847,-0.1093
6.4,1.4813,0.01507,0.00864,-0.1086
6.5,1.494,0.01524,0.00882,-0.108
6.6,1.5066,0.01541,0.009,-0.1073
6.7,1.5189,0.01559,0.0092,-0.1065
6.8,1.5312,0.01579,0.00941,-0.1058
6.9,1.5438,0.01595,0.00959,-0.105
7,1.5562,0.01613,0.00979,-0.1042
7.1,1.5685,0.01631,0.01,-0.1034
7.2,1.5806,0.0165,0.01021,-0.1025
7.3,1.5925,0.01671,0.01044,-0.1016
7.4,1.6042,0.01692,0.01068,-0.1007
7.5,1.6158,0.01714,0.01092,-0.0997
7.6,1.6277,0.01733,0.01116,-0.0988
7.7,1.6393,0.01754,0.0114,-0.0978
7.8,1.6508,0.01776,0.01165,-0.0967
7.9,1.662,0.01798,0.0119,-0.0956
8,1.6729,0.01822,0.01216,-0.0945
8.1,1.6834,0.01847,0.01245,-0.0933
8.2,1.6937,0.01873,0.01274,-0.0921
8.3,1.7042,0.01897,0.01303,-0.0908
8.4,1.7135,0.01922,0.01333,-0.0894
8.5,1.7224,0.01948,0.01365,-0.0879
8.6,1.7312,0.01976,0.01397,-0.0864
8.7,1.7398,0.02005,0.01431,-0.0849
8.8,1.7482,0.02036,0.01467,-0.0833
8.9,1.7564,0.02069,0.01504,-0.0818
9,1.7644,0.02104,0.01543,-0.0803
9.1,1.7728,0.02137,0.01582,-0.0788
9.2,1.7808,0.02173,0.01624,-0.0772
9.3,1.7887,0.0221,0.01668,-0.0757
9.4,1.7963,0.0225,0.01713,-0.0742
9.5,1.8037,0.02291,0.01761,-0.0726
9.6,1.8108,0.02334,0.01811,-0.0711
9.7,1.8176,0.0238,0.01863,-0.0696
9.8,1.8241,0.02428,0.01917,-0.068
9.9,1.8302,0.02479,0.01975,-0.0665
10,1.8357,0.02534,0.02036,-0.065
10.1,1.841,0.02591,0.02103,-0.0635
10.2,1.846,0.02652,0.02172,-0.062
10.3,1.8506,0.02717,0.02246,-0.0605
10.4,1.8547,0.02785,0.02323,-0.0591
10.5,1.8584,0.02858,0.02405,-0.0577
10.6,1.8615,0.02936,0.02492,-0.0564
10.7,1.8642,0.03018,0.02584,-0.0551
10.8,1.8665,0.03106,0.02681,-0.054
10.9,1.8683,0.03199,0.02783,-0.0529
11,1.8698,0.03297,0.02892,-0.052
11.2,1.8736,0.03498,0.03111,-0.0505
11.3,1.874,0.03614,0.03236,-0.0499
11.4,1.874,0.03737,0.03369,-0.0496
11.5,1.8736,0.03868,0.0351,-0.0494
11.6,1.8728,0.04008,0.0366,-0.0495
11.7,1.8709,0.04163,0.03825,-0.0498
11.8,1.868,0.04333,0.04007,-0.0504
11.9,1.8647,0.04516,0.04202,-0.0513
12,1.8609,0.04712,0.04409,-0.0526
12.1,1.8566,0.04923,0.04632,-0.0543
12.2,1.8519,0.0515,0.0487,-0.0563
12.4,1.8409,0.05658,0.054,-0.0616
12.6,1.8275,0.06246,0.0601,-0.0683
12.8,1.8107,0.06928,0.0671,-0.0765
13.4,1.7386,0.09359,0.09179,-0.1038
13.5,1.7265,0.09754,0.09577,-0.1083
13.6,1.7153,0.10139,0.09965,-0.1128
13.7,1.7047,0.10521,0.10349,-0.1172
13.9,1.6857,0.11249,0.11081,-0.1256
14,1.6775,0.116,0.11434,-0.1296
14.1,1.6701,0.11938,0.11773,-0.1334
14.2,1.6635,0.12266,0.12104,-0.1371
14.3,1.6585,0.1257,0.12408,-0.1405
14.4,1.6601,0.12762,0.126,-0.1423
14.5,1.6573,0.13028,0.12867,-0.1452
14.6,1.6479,0.13418,0.13258,-0.1497
14.7,1.6355,0.13874,0.13716,-0.1549
14.8,1.6244,0.14322,0.14166,-0.1599
14.9,1.6163,0.14721,0.14566,-0.1642
15,1.6113,0.15062,0.14909,-0.1678
15.1,1.6087,0.15354,0.15202,-0.1708
15.2,1.6077,0.15609,0.15458,-0.1733
15.3,1.6079,0.15839,0.15688,-0.1756
15.4,1.6129,0.1596,0.15809,-0.1767
15.5,1.6148,0.16148,0.15998,-0.1785
15.6,1.621,0.16237,0.16087,-0.1793
15.7,1.6243,0.16384,0.16235,-0.1808
15.8,1.6148,0.16878,0.16729,-0.1857
15.9,1.611,0.1724,0.17092,-0.1892
16,1.6101,0.17529,0.17382,-0.1919
16.1,1.611,0.17777,0.1763,-0.1942
16.2,1.6129,0.17987,0.17841,-0.1962
16.3,1.6154,0.18181,0.18036,-0.198
16.4,1.6184,0.18357,0.18212,-0.1997
16.5,1.6218,0.18523,0.18379,-0.2013
16.6,1.6255,0.18677,0.18534,-0.2027
16.7,1.6295,0.18821,0.18679,-0.2042
16.8,1.6336,0.18959,0.18818,-0.2055
16.9,1.6268,0.19539,0.19398,-0.2104
17,1.6283,0.19798,0.19657,-0.2127
17.1,1.6308,0.2002,0.1988,-0.2146
17.2,1.6339,0.20215,0.20077,-0.2164
17.3,1.6375,0.20388,0.2025,-0.218
17.4,1.6415,0.20543,0.20406,-0.2194
17.5,1.6459,0.20685,0.20549,-0.2208
17.6,1.6454,0.21061,0.20924,-0.2239
17.7,1.6466,0.21365,0.21229,-0.2264
17.8,1.6492,0.21607,0.21472,-0.2284
17.9,1.6524,0.21817,0.21683,-0.2303
18,1.6561,0.22004,0.2187,-0.2319
18.1,1.6601,0.22172,0.22039,-0.2335
18.2,1.6643,0.22329,0.22197,-0.2349
18.3,1.6651,0.22691,0.22558,-0.2378
18.4,1.6673,0.22987,0.22855,-0.2401
18.5,1.6708,0.23199,0.23068,-0.242
18.6,1.6745,0.23396,0.23265,-0.2437
18.7,1.6783,0.23585,0.23455,-0.2453
18.8,1.6822,0.23767,0.23637,-0.247
18.9,1.6845,0.24065,0.23935,-0.2494
19,1.6874,0.24328,0.24199,-0.2515
19.1,1.6905,0.24576,0.24447,-0.2535
19.2,1.6939,0.24812,0.24684,-0.2555
19.3,1.6973,0.25041,0.24912,-0.2574
19.4,1.7009,0.25261,0.25134,-0.2592
19.5,1.7046,0.25476,0.25349,-0.2611
19.6,1.7083,0.25688,0.25561,-0.2629
19.7,1.7121,0.25893,0.25766,-0.2646
19.8,1.7159,0.26094,0.25968,-0.2664
19.9,1.7198,0.26295,0.26169,-0.2681
20,1.7229,0.2657,0.26444,-0.2703
20.1,1.726,0.26851,0.26725,-0.2725
20.2,1.7294,0.27109,0.26983,-0.2746
20.3,1.7329,0.27363,0.27238,-0.2766
20.4,1.7365,0.27611,0.27486,-0.2786
20.5,1.7401,0.27852,0.27727,-0.2805
20.6,1.7438,0.28089,0.27965,-0.2825
20.7,1.7475,0.28323,0.28199,-0.2844
20.8,1.7512,0.28554,0.28431,-0.2863
20.9,1.755,0.28782,0.28659,-0.2882
21,1.7588,0.29006,0.28884,-0.29
21.2,1.7666,0.29417,0.29295,-0.2936
21.3,1.7704,0.29631,0.2951,-0.2954
21.4,1.7742,0.29844,0.29723,-0.2973
21.5,1.778,0.30054,0.29934,-0.2991
21.6,1.7815,0.30342,0.30221,-0.3013
21.7,1.7851,0.30674,0.30553,-0.3037
21.8,1.7888,0.30953,0.30832,-0.3058
21.9,1.7926,0.31219,0.31098,-0.3078
22,1.7964,0.31485,0.31364,-0.3099
22.1,1.8002,0.31737,0.31617,-0.3119
22.2,1.804,0.31987,0.31867,-0.3138
22.3,1.8079,0.32235,0.32116,-0.3158
22.4,1.8117,0.32478,0.32359,-0.3178
22.5,1.8155,0.3272,0.32601,-0.3197
22.6,1.8193,0.32959,0.32841,-0.3217
22.7,1.8231,0.33197,0.33079,-0.3237
22.8,1.8269,0.33433,0.33315,-0.3256
22.9,1.8307,0.33667,0.3355,-0.3275
23,1.8345,0.33901,0.33784,-0.3295
23.1,1.8383,0.34134,0.34017,-0.3315
23.2,1.842,0.34365,0.34249,-0.3334
23.3,1.8458,0.34595,0.34479,-0.3354
23.4,1.8495,0.34824,0.34708,-0.3373
23.5,1.8532,0.35054,0.34939,-0.3393
23.6,1.8569,0.35281,0.35166,-0.3412
23.7,1.8605,0.35509,0.35394,-0.3432
23.8,1.8642,0.35735,0.35621,-0.3452
23.9,1.8678,0.35961,0.35847,-0.3472
24,1.8714,0.36187,0.36075,-0.3491
24.1,1.875,0.36411,0.36298,-0.3511
24.2,1.8793,0.36784,0.36671,-0.3536
24.3,1.8841,0.37266,0.37153,-0.3563
24.4,1.888,0.37543,0.3743,-0.3585
24.5,1.8919,0.3782,0.37707,-0.3606
24.6,1.8957,0.38088,0.37976,-0.3627
24.7,1.8995,0.38356,0.38244,-0.3648
24.8,1.9032,0.38615,0.38504,-0.3669
24.9,1.907,0.3888,0.38768,-0.3691
25,1.9107,0.39138,0.39027,-0.3712
25.1,1.9144,0.39398,0.39287,-0.3733
25.2,1.918,0.39654,0.39543,-0.3754
25.3,1.9217,0.3991,0.398,-0.3775
25.4,1.9253,0.40165,0.40055,-0.3796
25.5,1.9289,0.40419,0.40309,-0.3817
25.6,1.9325,0.40671,0.40562,-0.3839
25.7,1.9361,0.40925,0.40816,-0.386
25.8,1.9397,0.41177,0.41069,-0.3881
25.9,1.9432,0.41427,0.41319,-0.3902
26,1.9467,0.41679,0.41572,-0.3924
26.1,1.9502,0.4193,0.41823,-0.3945
26.2,1.9536,0.42178,0.42072,-0.3967
26.3,1.957,0.42428,0.42322,-0.3988
26.4,1.9604,0.42675,0.4257,-0.401
26.5,1.9638,0.42925,0.42819,-0.4031
26.6,1.9672,0.43173,0.43068,-0.4053
26.7,1.9705,0.43418,0.43314,-0.4075
26.8,1.9738,0.43664,0.4356,-0.4097
26.9,1.9771,0.4391,0.43806,-0.4119
27,1.9817,0.44349,0.44245,-0.4145
27.1,1.9859,0.44721,0.44617,-0.417
27.2,1.9897,0.45047,0.44944,-0.4194
27.3,1.9935,0.45369,0.45265,-0.4217
27.4,1.9978,0.45761,0.45658,-0.4243
27.5,2.0013,0.46051,0.45948,-0.4266
27.6,2.0048,0.46341,0.46239,-0.4289
27.7,2.0082,0.46629,0.46527,-0.4312
27.8,2.0116,0.4691,0.46809,-0.4335
27.9,2.015,0.47196,0.47095,-0.4358
28,2.0183,0.4748,0.47379,-0.4382
28.1,2.0217,0.47764,0.47663,-0.4405
28.2,2.025,0.48042,0.47942,-0.4428
28.3,2.0282,0.48323,0.48223,-0.4451
28.4,2.0315,0.48603,0.48503,-0.4475
28.5,2.0347,0.4888,0.48781,-0.4498
28.6,2.0378,0.49157,0.49058,-0.4521
28.7,2.041,0.49434,0.49335,-0.4544
29,2.0455,0.51562,0.51466,-0.4612
29.1,2.0489,0.51788,0.51692,-0.4637

