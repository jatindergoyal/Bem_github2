airfoilName st4.dat
Re 	3.000000e+04
M 	5.000000e-01
N 	4.500000e+00
VACC 	0 VACCflag 1
XTR 	1.00 	1.00 XTRflag 0
alpha,cl,cd,cdp,cm
-1.6,0.0126,0.01475,-8e-05,-0.0453
-1.5,0.0214,0.01437,-0.00039,-0.0438

