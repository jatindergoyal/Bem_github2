# Blade Element Momentum Theory for Positive and Negative Thrust Propellers
[![Open in MATLAB Online](https://www.mathworks.com/images/responsive/global/open-in-matlab-online.svg)](https://matlab.mathworks.com/open/github/v1?host=gitlab.tudelft.nl&repo=jatindergoyal/Bem_github2)

Blade Element Momentum Theory (BEMT) code for low-fidelity aerodynamic analysis of propellers operating in propulsive and regenerative regimes. This code is meant to be used for uniform inflow conditions with the propeller axis aligned with the inflow. Polar data is required as the input for the code in a specific format, as exemplified in the `./data/PolarData/` folder.


## Requirements

The code makes use of [cosspace.m](https://nl.mathworks.com/matlabcentral/fileexchange/5491-cosspace) version 1.3.0.1 developed by Cole Stephens from MathWorks (see more in the **License** section).

This code has been developed in MATLAB R2021a, and tested in both MATLAB R2021a and R2023a. 


## Installation

No additional installation is required for the code except an activated installation of MATLAB (preferably 2021a).


## Structure

```
.
├── CITATION.cff
├── data
│   ├── PolarData
│   │   ├── st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.00
│   │   ├── st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.05
│   │   ├── st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.25
│   │   ├── st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.50
│   │   ├── st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.70
│   ├── PropellerGeometry
│   │   └── XProp_geometry.csv
│   └── AirDensityViscosity.xlsx
├── LICENSES  
│   ├── Apache-License-v2.0.txt
│   ├── CC-BY-4.0.txt
│   └── license_cosspace.txt
├── outputs
├── README.md
└── src
    ├── airProp.m   
    ├── BEMfunctionV03.m
    ├── BEMfunctionV03_Minterp.m
    ├── cosspace.m
    ├── importfile.m
    ├── MainFile.m
    ├── opPoint.m
    ├── polarInterp4D_viterna.m
    ├── polarProcess_Minterp.m
    ├── polarProcess_viterna.m
    ├── preProcessBEMV03.m
    ├── processBEMResult.m
    ├── UserInput.m
    └── viternaExtrap.m

```

The `./data/PropellerGeometry/` directory contains the propeller geometry file determining the blade properties (`XProp_geometry.csv`). This file contains the following variables in the specified order:
- `Station` is the name of different sections of the blades.
- `R/Rp` is the non-dimensionalized radial location of the station, normalized with propeller radius. 
- `R[mm]` is the dimensionalized radial location of the station in millimeters.
- `Twist[deg]` is the twist of the blade sections in degrees with respect to the reference location (0.7R for the example geometry).
- `Chord[mm]` is the local chord size of the blade sections in millimeters.

The `./data/PolarData/` directory contains the example polar data, which are used to calculate the aerodynamic performance of the blade. The sub-directories contain files with the naming convention: `st<station_number>_Re<Reynolds_number>_N<critical_amplification_exponent>_xTRU<transition_location_on_upper_surface>_xTRL<transition_location_on_lower_surface>_f<rotational_correction_parameter>_rf123_<rotation_correction_parameter_1>_<rotation_correction_parameter_2>_<rotation_correction_parameter_3>.txt`.

By default, if `PG.method` in `UserInput.m` file is defined to be `PG` (**Prandtl-Glauert**) or `KT` (**Karman-Tsien**), then the polar data in the `./data/PolarData/st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.00` directory is used for the calculations. The resulting interpolated polar data is saved as `./data/PolarData/st2-25_Re10000-990000_N450_xTRU1.00_xTRL1.00_f1.00_cr2by3_1.00-0.07_rf123_1.00_1.00_0.00_M0.00/InterpolatedData_Re50000-990000_alpha-50-50_viterna.mat`. On the other hand, if `PG.method` in the `UserInput.m` file is defined to be `interp`, then all the polar data in the `./data/PolarData/` directory is used by the code. The resulting interpolated polar data is saved as  `./data/PolarData/InterpolatedData_Re50000-990000_M0.00-0.70_alpha-50-50_viterna.mat`

The **UserInput.m** file in `./src/` is the file where ambient conditions, propeller parameters, operating conditions, convergence conditions, BEM corrections, polar data, and output file are specified.   

The **Mainfile.m** in `./src/` is the script to run. With the default settings in the code, the results will be saved in a mat file in the specified output folder (by default, in the `./outputs/` directory). The results of the BEMT code are contained in a structure array (struct) named `bem`. The `bem` struct contains the following variables:

- `Uinf`, `omega`, `n`, and `J` refer to the **freestream velocity** in [m/s], **rotational speed** in [rad/s], **rotational speed** in [1/s] and **advance ratio** [-] respectively for each operating condition, expected size = (no. of operating conditions X 1).  
- `a` and `a1` refer to the **axial induction** [-] and **tangential induction** [-] respectively, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `iter` refers to the no. of iterations to reach convergence, expected size = (no. of operating conditions X 1).  
- `diff` and `diff1` refer to the final difference between the last two iteration inductions, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `V_a`, `V_t`, and `V_p` refer to the **axial velocity**, **tangential velocity**, and **effective velocity** in [m/s] respectively, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `phi` and `alpha` refer to **inflow angle** and **angle of attack** in [deg] respectively, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `M` and `Re` refer to **Mach no.** and **Reynolds no.** respectively calculated based on the local operating conditions at the blade sections, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `Cl` and `Cd` refer to **lift coefficient** and **drag coefficient** respectively, calculated based on the local operating conditions, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `dT` and `dQ` refer to **thrust** in [N] and **torque** in [Nm], respectively, at each annulus, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `dTNdr` and `dQNdr` refer to **thrust per blade per unit span** in [N/m] and **torque per blade per unit span** in [Nm/m] respectively at each annulus, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `Ct` and `Cp` refer to local **thrust coefficient** and **power coefficient** based on rotational speed, respectively, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `dTc` and `dPc` refer to local **thrust coefficient** and **power coefficient** based on freestream velocity, respectively, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `F` refers to the root and tip correction factor based on Prandtl's tip loss theory, expected size = (no. of operating conditions X no. of elements in the radial direction).  
- `T`, `Q`, and `P` refer to integrated **thrust** [N], **torque** [Nm], and **power** [W] for each operating condition, expected size = (no. of operating conditions X 1).  
- `CT`, `CQ`, and `CP` refer to integrated **thrust coefficient**, **torque coefficient**, and **power coefficient** based on rotational speed for each operating condition, expected size = (no. of operating conditions X 1).  
- `TC` and `PC` refer to integrated **thrust coefficient** and **power coefficient** based on freestream velocity for each operating condition, expected size = (no. of operating conditions X 1).  
- `eff` and `eff_regen` refer to the propeller and energy-harvesting efficiency for each operating condition, expected size = (no. of operating conditions X 1).  


## Usage 

To run this code:

- Setup the case in **UserInput.m** file. 
- Evaluate your case by running **Mainfile.m** file in Matlab.

When running the **Mainfile.m** with the **UserInput.m** as provided, you should see a new directory in `./outputs/` called `pitch15-15_V30-30_J0.50-1.50_n2953-8858_B3_SF1_PG` with two files: `BemResults_N450_xtr1.00.mat` with the BEM outputs, and `D4_BemResults_N450_xtr1.00.mat` with the interpolated polar data.


## Author(s)

This software has been developed by
**Jatinder Goyal** ![ORCID logo](https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png) [0000-0001-7448-8617](https://orcid.org/0000-0001-7448-8617), Technische Universiteit Delft


## License

The function `cosspace` in the `src/` directory is licensed under **license_cosspace** (see [license_cosspace](LICENSES/license_cosspace.txt) file) with copyright &copy; 2016, The MathWorks, Inc. *All rights reserved*. 

The contents in the `data/` directory are licensed under a **CC-BY 4.0** (see [CC-BY-4.0](LICENSES/CC-BY-4.0.txt) file). The source code and any other file in this repository are licensed under an **Apache License v2.0** (see [Apache-License-v2.0](LICENSES/Apache-License-v2.0.txt) file).

Copyright notice:

Technische Universiteit Delft hereby disclaims all copyright interest in the program “Blade Element Momentum Theory for Positive and Negative Thrust Propellers” (meaning the source code files licensed under **Apache License v2.0** as explained above). It is Matlab code to calculate the aerodynamic performance of propellers operating in propulsive and regenerative regimes, written by the Author(s).    
Henri Werij, Dean of Faculty of Aerospace Engineering, Technische Universiteit Delft.

&copy; 2023, J. Goyal


## References

- Rwigema, M. K. (2010, September). Propeller blade element momentum theory with vortex wake deflection. In 27th International congress of the aeronautical sciences (Vol. 1, pp. 727-735).  
- Mahmuddin, F., Klara, S., Sitepu, H., & Hariyanto, S. (2017). Airfoil lift and drag extrapolation with viterna and montgomerie methods. Energy Procedia, 105, 811-816.  
- The Engineering ToolBox (2003). U.S. Standard Atmosphere vs. Altitude. [online] Available at: [https://www.engineeringtoolbox.com/standard-atmosphere-d_604.html](https://www.engineeringtoolbox.com/standard-atmosphere-d_604.html).  
- Cole Stephens (2023). cosspace [https://www.mathworks.com/matlabcentral/fileexchange/5491-cosspace](https://www.mathworks.com/matlabcentral/fileexchange/5491-cosspace), MATLAB Central File Exchange. Retrieved November 6, 2023.  


## Cite this repository

If you use this software, please cite it as below or check out the `CITATION.cff` file.

**How to cite this repository**: Goyal, J., 2023. Blade Element Momentum Theory for Positive and Negative Thrust Propellers. 4TU.ResearchData. Software. [https://doi.org/10.4121/e748a68d-5f15-4757-b904-880bcec8217b](https://doi.org/10.4121/e748a68d-5f15-4757-b904-880bcec8217b) 


## Would you like to contribute?

If you have any comments, feedback, or recommendations, feel free to **reach out** by sending an email to jatindergoel777@gmail.com

If you want to contribute directly, you are welcome to **fork** this repository.

Thank you, and enjoy!

